FROM python:3.11-slim-buster
LABEL authors="Stefan Kohlbacher"

WORKDIR /usr/app/src

COPY main.py ./
COPY data ./data
COPY modules ./modules
COPY requirements.txt ./

RUN ["python", "-m", "pip", "install", "--upgrade", "pip"]
RUN ["pip", "install", "-r", "requirements.txt"]
#RUN ["apt-get", "update"]
#RUN ["apt-get", "upgrade", "-y"]
#RUN ["apt-get", "install", "nano", "-y"]

ENTRYPOINT ["python", "./main.py", "-d"]