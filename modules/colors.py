# ANSI Stuff
PREF = "\033["
RESET = f"{PREF}0m"

# Color Codes
BLACK = "30m"
ERROR = "31m"
GREEN = "32m"
MESSAGE = "33m"
LOGO = "34m"
MAGENTA = "35m"
USER = "36m"
WHITE = "37m"