import os
import os.path
from random import randint


def randMeme():
    ordner = randint(1, 7)
    pathOrdner = "data/memes/" + str(ordner) + "/"

    numMemes = len([name for name in os.listdir(pathOrdner) if os.path.isfile(os.path.join(pathOrdner, name))])
    memeList = [name for name in os.listdir(pathOrdner) if os.path.isfile(os.path.join(pathOrdner, name))]

    randomMeme = randint(1, numMemes)
    meme = [i for i in memeList if i.startswith(str(randomMeme))]
    memePath = pathOrdner + meme[0]

    return memePath


def randZeit():
    ordner = randint(1, 2)
    pathOrdner = "data/zeitschriften/" + str(ordner) + "/"

    numZeit = len([name for name in os.listdir(pathOrdner) if os.path.isfile(os.path.join(pathOrdner, name))])
    zeitList = [name for name in os.listdir(pathOrdner) if os.path.isfile(os.path.join(pathOrdner, name))]

    randomZeit = randint(1, numZeit)
    zeitung = [i for i in zeitList if i.startswith(str(randomZeit))]
    zeitungPath = pathOrdner + zeitung[0]

    return zeitungPath

