# AHBG bot

Der ganze Stolz der AHBG Inc.



## Anwendung

Vor dem ersten starten muss der Bot konfiguriert werden, siehe [Setup](#setup) und [Konfiguration](#konfiguration)

Der Bot kann entweder in "[Skript Modus](#skript-modus)", als normales Python Skript, oder "[Docker Modus](#docker-modus)", als Docker Container, ausgeführt werden.

Um alle Befehle des Bots zu sehen, muss einfach (wenn der Bot bereits in Betrieb ist!) das folgende auf Discord gesendet werden:

````
!help
````

### Skript Modus

Um den Bot zu starten muss einfach der folgende Befehl ausgeführt werden:

```bash
python main.py --TOKEN "<Discord Bot Token>"
```

### Docker Modus

Achtung: In diesem Modus kann der Bot nur durch Umgebungsvariablen Konfiguriert werden. Um Docker Compose zu verwenden, sollte eine "token.env" Datei, welche den Bot Token enthält, im "data" Ordner erstellt werden.

Der Bot kann mit dem folgenden Befehl gestartet werden:

```bash
docker run \
    -e DC_TOKEN='TOKEN' \
    hornybot
```

## Setup

Vor dem Starten muss eine config.yaml Datei im "data" Ordner erstellt werden.

Zusätzlich wird ein Discord-Bot Token benötigt, eine englische Anleitung kann [hier](https://www.digitaltrends.com/gaming/how-to-make-a-discord-bot/) gefunden werden (Schritte 2-4). 

Vor dem ersten Ausführen müssen die notwendigen Pakete installiert werden:

```shell
pip install -r requirements.txt
```

## Konfiguration

### Flags

| Flag    | Beschreibung                                                                                | Standard Wert |
|---------|---------------------------------------------------------------------------------------------|---------------|
| -d      | Starten den Bot in "Docker Mode" (siehe Oben)                                               | False         |
| --TOKEN | Optional: Der Token kann auch über die Kommandozeile übergeben werden, statt der YAML Datei | None          |

### YAML Datei

#### Discord

| Option | Beschreibung                                                                            | Standard Wert |
|--------|-----------------------------------------------------------------------------------------|---------------|
| token  | The Discord Bot Token, ist nur notwendig wenn er nicht bereits als Flag übergeben wurde | None          |

#### Beispiel Konfiguration

```yaml
discord:
	token: "<Discord Bot Token>"
```

### Umgebungsvariablen

Werden nur im Docker Modus verwendet.

| Variable | Wert              | Beschreibung | Standard Wert |
|----------|-------------------|--------------|---------------|
| DC_TOKEN | Discord Bot Token | Bot Token    | None          |

