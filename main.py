# Necessary python packages
try:
    import discord
    import os
    import yaml
    import argparse
    import logging

    from yaml.loader import SafeLoader

except ModuleNotFoundError as e:
    # ANSI Stuff
    pref = "\033["
    reset = f"{pref}0m"
    error = "31m"
    warning = "33m"

    print(f'{pref}0;{warning}' + 'Alert: ' + reset + 'a required package is not installed!')
    quit(-1)

# Custom modules
try:
    from modules import utils
    from modules import colors
    from modules import helper
    from modules import imageSelector as IS
    from modules import quoteSelector as QS

except ModuleNotFoundError as e:
    # ANSI Stuff
    pref = "\033["
    reset = f"{pref}0m"
    error = "31m"
    warning = "33m"

    print(f'{pref}0;{error}' + 'Error: ' + reset + 'module missing! Please download it from the following gitlab page: '
                                                   'https://gitlab.com/Mr_Kohli/ahbg-bot')

    quit(-1)

# Discord Client
intents = discord.Intents.all()
intents.typing = True
intents.presences = True
client = discord.Client(intents=intents)
config = [
    None  # Discord Token
]


def initializer():
    # --- Parse Flags ---
    parser = argparse.ArgumentParser()
    # Add arguments
    parser.add_argument('--TOKEN')
    parser.add_argument('-d', action='store_true')
    # parse the arguments
    args = parser.parse_args()
    # Init logging
    logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s: %(message)s')

    # Check if the bot is started in docker mode
    if args.d:
        try:
            config[0] = os.environ.get('DC_TOKEN')  # get bot token
        except AttributeError as e:
            logging.error('Invalid environment variables\nPlease see the README for help')
            quit(-1)

        if config[0] is None:
            logging.error("Discord Bot Token is missing! Please see the README for help")
            quit(-1)

        # Start bot
        client.run(config[0])

    if not os.path.isfile('data/config.yaml'):
        logging.error('config.yaml is missing!\nPlease see the README for help')
        quit(-1)

    with open("data/config.yaml", 'r') as f:
        data = yaml.load(f, Loader=SafeLoader)

    # --- get discord Token ---
    # Check if the token is set in the arguments and use it
    if args.TOKEN is not None:
        config[0] = args.TOKEN
    else:
        try:
            config[0] = data['discord']['token']
        except KeyError:
            logging.error('config.yaml not formatted right!\nPlease see the README for help')
            quit(-1)
        if config[0] is None or config[0] == "":
            logging.error('Discord Bot Token is missing!\nPlease see the README for help')
            quit(-1)

    # Start bot
    client.run(config[0])


def message_interpreter(messageSend):
    if any('!meme' in s for s in messageSend):
        return "meme"
    elif any('!zeitung' in s for s in messageSend):
        return "zeitung"
    elif any('!zitat' in s for s in messageSend):
        return "zitat"
    elif any('!help' in s for s in messageSend):
        return "hilfe"


@client.event
async def on_ready():
    utils.clear_console()
    logo1 = "           _    _ ____   _____     _____ _   _  _____       ____   ____ _______ \n"
    logo2 = "     /\   | |  | |  _ \ / ____|   |_   _| \ | |/ ____|     |  _ \ / __ \__   __|\n"
    logo3 = "    /  \  | |__| | |_) | |  __      | | |  \| | |          | |_) | |  | | | |   \n"
    logo4 = "   / /\ \ |  __  |  _ <| | |_ |     | | | . ` | |          |  _ <| |  | | | |   \n"
    logo5 = "  / ____ \| |  | | |_) | |__| |    _| |_| |\  | |____ _    | |_) | |__| | | |   \n"
    logo6 = " /_/    \_\_|  |_|____/ \_____|   |_____|_| \_|\_____(_)   |____/ \____/  |_|   \n"
    logging.info(f'{colors.PREF}0;{colors.LOGO}' + logo1 + logo2 + logo3 + logo4 + logo5 + logo6 + colors.RESET)
    await client.change_presence(
        activity=discord.Activity(type=discord.ActivityType.watching, name=' sich die neue HTL Krone an'))
    logging.info(
        'Bot is now ' + f'{colors.PREF}0;{colors.GREEN}' + 'online' + colors.RESET + ' as ' + f'{colors.PREF}0;{colors.USER}' +
        '{0.user}'.format(client) + colors.RESET)


@client.event
async def on_message(message):
    if message.author == client.user:
        return

    messageSend = message.content.lower()
    messageSend = messageSend.split(' ')

    command = message_interpreter(messageSend)

    match command:
        case "meme":
            f = open(IS.randMeme(), "rb")
            meme = discord.File(f)

            msg = "Einer der sehr sehr guten Memes wird gesucht."
            await message.channel.send(msg)
            await message.channel.send(file=meme)

            logging.info(f'{colors.PREF}0;{colors.USER}' + str(message.author) + colors.RESET + " will Maimais sehen")

        case "zeitung":
            f = open(IS.randZeit(), "rb")
            zeitung = discord.File(f)

            msg = "Suche die guhte Fachlektüre"
            await message.channel.send(msg)
            await message.channel.send(file=zeitung)

            logging.info(f'{colors.PREF}0;{colors.USER}' + str(message.author) + colors.RESET +
                         " braucht etwas mentale Stimulation")

        case "zitat":
            zitat = QS.randQuote()
            msg = "```" + zitat + "```"
            await message.channel.send(msg)

            logging.info(f'{colors.PREF}0;{colors.USER}' + str(message.author) + colors.RESET +
                         " wollte das folgende Zitat sehen:\n" + f'{colors.PREF}0;{colors.MESSAGE}' +
                         zitat + colors.RESET)

        case "hilfe":
            msg = helper.generateHelp()
            await message.channel.send(msg)

            logging.info(f'{colors.PREF}0;{colors.USER}' + str(message.author) + colors.RESET +
                         " braucht dringent Hilfe\n")


if __name__ == '__main__':
    initializer()
